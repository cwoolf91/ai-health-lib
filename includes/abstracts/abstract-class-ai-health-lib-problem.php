<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://axiosinteractive.net
 * @since      1.0.0
 *
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Ai_Health_Lib
 * @subpackage Ai_Health_Lib/public
 * @author     Chris Woolf <chris.woolf91@gmail.com>
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
 
abstract class Ai_Health_Lib_Problem_Abs {
	
	/**
	* These are protected base variables used for MedlinePlus
	* Concatenate code variables and code system variables to these.
	*
	*
	*/
	protected $base_medline_url;
	protected $base_code_system;
	protected $base_actual_code;
	protected $selected_code_system;
	protected $actual_code;
	protected $base_name_url;
	protected $problem_code_name;
	
	// For ICD-10-CM use:
	protected $ICD_10_CM;
	// For ICD-9-CM use:
	protected $ICD_9_CM;
	// For SNOMED CT use:
	protected $SNOMED_CT;
	
	// Default English.
	protected $base_language_selection;
	protected $language_selection;
	
	// Response Type. Default is XML
	protected $base_response_type;
	protected $response_type_xml;
	protected $response_type_json;
	
	// Problem code json obj
	protected $problem_code_json_obj;
	
	/**
	 * Set the base vars for using Medline Plus API. Need to sanitize.
	 *
	 * @since    1.0.0
	 */
	
	abstract protected function initialize_base_vars();
	
	/**
	 * Set the default vars for using Medline Plus API. This is not the same as base vars. Need to sanitize.
	 *
	 * @since    1.0.0
	 */
	
	abstract protected function initialize_default_vars();
	
	/**
	 * Set default response type to XML. Need to sanitize.
	 *
	 * @since    1.0.0
	 */
	
	abstract protected function set_default_response_type();
	
	/**
	 * Basic GET function. Testing
	 *
	 * @since    1.0.0
	 */
	
	abstract public function get_problem_response( $code_system, $code_input, $post_id );
	
	/**
	 * Code input validation function.
	 *
	 * @since    1.0.0
	 */
	 
	abstract protected function set_actual_code( $code_to_set );
	
	/**
	 * Setter for code system
	 * 
	 * @since    1.0.0
	 */
	
	abstract protected function set_actual_code_system( $code_system );

}
