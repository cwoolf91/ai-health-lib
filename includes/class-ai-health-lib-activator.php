<?php

/**
 * Fired during plugin activation
 *
 * @link       https://axiosinteractive.net
 * @since      1.0.0
 *
 * @package    Ai_Health_Lib
 * @subpackage Ai_Health_Lib/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Ai_Health_Lib
 * @subpackage Ai_Health_Lib/includes
 * @author     Chris Woolf <chris.woolf91@gmail.com>
 */
class Ai_Health_Lib_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
