<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://axiosinteractive.net
 * @since      1.0.0
 *
 * @package    Ai_Health_Lib
 * @subpackage Ai_Health_Lib/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Ai_Health_Lib
 * @subpackage Ai_Health_Lib/admin
 * @author     Chris Woolf <chris.woolf91@gmail.com>
 */
 
  if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
 
class Ai_Health_Lib_Admin {
	
	/**
	 * The options name to be used in this plugin
	 *
	 * @since  	1.0.0
	 * @access 	private
	 * @var  	string 		$option_name 	Option name of this plugin
	 */
	private $option_name = 'ai_health_lib';

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ai_Health_Lib_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ai_Health_Lib_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/ai-health-lib-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ai_Health_Lib_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ai_Health_Lib_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/ai-health-lib-admin.js', array( 'jquery' ), $this->version, false );

	}
	
	/**
	 * Add an options page under the Settings submenu
	 *
	 * @since  1.0.0
	 */
	public function add_options_page() {
	
		$this->plugin_screen_hook_suffix = add_options_page(
			__( 'Health Library Settings', 'ai-health-lib' ),
			__( 'Health Library', 'ai-health-lib' ),
			'manage_options',
			$this->plugin_name,
			array( $this, 'display_options_page' )
		);
	}
	
	/**
	 * Render the options page for plugin
	 *
	 * @since  1.0.0
	 */
	public function display_options_page() {
		include_once 'partials/ai-health-lib-admin-display.php';
	}
	
	public function register_setting() {
		// Add a General section
		add_settings_section(
			$this->option_name . '_general',
			__( 'General', 'ai-health-lib' ),
			array( $this, $this->option_name . '_general_cb' ),
			$this->plugin_name
		);
		
		add_settings_field(
			$this->option_name . '_position',
			__( 'Text position', 'ai-health-lib' ),
			array( $this, $this->option_name . '_position_cb' ),
			$this->plugin_name,
			$this->option_name . '_general',
			array( 'label_for' => $this->option_name . '_position' )
		);
		
		add_settings_field(
			$this->option_name . '_day',
			__( 'Post is outdated after', 'ai-health-lib' ),
			array( $this, $this->option_name . '_day_cb' ),
			$this->plugin_name,
			$this->option_name . '_general',
			array( 'label_for' => $this->option_name . '_day' )
		);
		
		add_settings_field(
			$this->option_name . '_code_system',
			__( 'Code System To Use', 'ai-health-lib' ),
			array( $this, $this->option_name . '_code_system_cb' ),
			$this->plugin_name,
			$this->option_name . '_general',
			array( 'label_for' => $this->option_name . '__code_system' )
		);
		register_setting( $this->plugin_name, $this->option_name . '_position', array( $this, $this->option_name . '_sanitize_position' ) );
		register_setting( $this->plugin_name, $this->option_name . '_day', 'intval' );
		register_setting( $this->plugin_name, $this->option_name . '_code_system', array( $this, $this->option_name . '_sanitize_code_system' ) );
	}
	
	/**
	 * Render the text for the general section
	 *
	 * @since  1.0.0
	 */
	public function ai_health_lib_general_cb() {
		echo '<p>' . __( 'Please change the settings accordingly.', 'ai-health-lib' ) . '</p>';
	}
	
	/**
	 * Render the radio input field for position option
	 *
	 * @since  1.0.0
	 */
	public function ai_health_lib_position_cb() {
		$position = get_option( $this->option_name . '_position' );
		?>
			<fieldset>
				<label>
					<input type="radio" name="<?php echo $this->option_name . '_position' ?>" id="<?php echo $this->option_name . '_position' ?>" value="before" <?php checked( $position, 'before' ); ?>>
					<?php _e( 'Before the content', 'ai-health-lib' ); ?>
				</label>
				<br>
				<label>
					<input type="radio" name="<?php echo $this->option_name . '_position' ?>" value="after" <?php checked( $position, 'after' ); ?>>
					<?php _e( 'After the content', 'ai-health-lib' ); ?>
				</label>
			</fieldset>
		<?php
	}
	
	/**
	 * Render the threshold day input for this plugin
	 *
	 * @since  1.0.0
	 */
	public function ai_health_lib_day_cb() {
		$day = get_option( $this->option_name . '_day' );
		echo '<input type="text" name="' . $this->option_name . '_day' . '" id="' . $this->option_name . '_day' . '"> '. __( 'days', 'ai-health-lib' );
	}
	
	/**
	 * Sanitize the text position value before being saved to database
	 *
	 * @param  string $position $_POST value
	 * @since  1.0.0
	 * @return string           Sanitized value
	 */
	public function ai_health_lib_sanitize_position( $position ) {
		if ( in_array( $position, array( 'before', 'after' ), true ) ) {
	        return $position;
	    }
	}
	
	/**
	 * Render the radio input field for selected code system option
	 *
	 * @since  1.0.0
	 */
	public function ai_health_lib_code_system_cb() {
		$code_system = get_option( $this->option_name . '_code_system' );
		?>
			<fieldset>
				<label>
					<input type="radio" name="<?php echo $this->option_name . '_code_system' ?>" id="<?php echo $this->option_name . '_code_system' ?>" value='1' <?php checked( $code_system, 1 ); ?>>
					<?php _e( 'Code System: ICD-10-CM', 'ai-health-lib' ); ?>
				</label>
				<br>
				<label>
					<input type="radio" name="<?php echo $this->option_name . '_code_system' ?>" value='2' <?php checked( $code_system, 2 ); ?>>
					<?php _e( 'Code System: ICD-9-CM', 'ai-health-lib' ); ?>
				</label>
				<label>
					<input type="radio" name="<?php echo $this->option_name . '_code_system' ?>" value='3' <?php checked( $code_system, 3 ); ?>>
					<?php _e( 'Code System: SNOMED-CT', 'ai-health-lib' ); ?>
				</label>
			</fieldset>
		<?php
	}
	
	/**
	 * Sanitize the text code system value before being saved to database
	 *
	 * @param  string $code_system $_POST value
	 * @since  1.0.0
	 * @return string           Sanitized value
	 */
	public function ai_health_lib_sanitize_code_system( $code_system ) {
		if ( in_array( $code_system, array( '1', '2', '3' ), true ) ) {
	        return $code_system;
	    }
	}

}
