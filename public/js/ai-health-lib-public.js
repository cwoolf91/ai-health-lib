(function( $ ) {
	'use strict';
	

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practice to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practicing this, we should strive to set a better example in our own work.
	 */
	 
	 // this happens when the DOM is loaded.
	 //$(function() {
	 //	 
	 //});
	 
	//alert( 'Here is the query string test\n\n');
	$( window ).load(function() {
		// Handling inputs for first form.
		$( '#ai-lib-form-1' ).submit(function(e) {
			e.preventDefault();
		
			var formSettings = {
				aihidden : '',
				codesystem :'',
				codeinput :'',
				action:''
			};
			formSettings.aihidden = $( "input[name='ai-health-lib-hidden']" ).val();
			formSettings.codesystem = $( "input[name='code-system-prob']:checked" ).val();
			formSettings.codeinput = $( "input[name='code-input-prob']" ).val();
			formSettings.action = 'ai_problem_test';
			
			$.ajax({
				url: lib_ajax.ajaxurl,
				type: 'POST',
				data: formSettings,
				success: function (response) {
					$( '#ai-health-lib-response' ).replaceWith(response);
				},
				error: function(response) {
					$( '#ai-lib-form-1' ).reset();
				}
			});
			
		});
		
		// Handling inputs for second form.
		$( '#ai-lib-form-2' ).submit(function(e) {
			e.preventDefault();
		
			var formSettings = {
				aihidden : '',
				codesystem :'',
				codeinput :'',
				nameinput:'',
				action:''
			};
			formSettings.aihidden = $( "input[name='ai-health-lib-hidden']" ).val();
			formSettings.codesystem = $( "input[name='code-system-drug']:checked" ).val();
			formSettings.codeinput = $( "input[name='code-input-drug']" ).val();
			formSettings.nameinput = $( "input[name='name-input-drug']" ).val();
			formSettings.action = 'ai_drug_test';
			
			
			$.ajax({
				url: lib_ajax.ajaxurl,
				type: 'POST',
				data: formSettings,
				success: function (response) {
					$( '#ai-health-lib-response' ).replaceWith(response);
				},
				error: function(response) {
					$( '#ai-lib-form-2' ).reset();
				}
			});
			
		});
		
		// Handling inputs for third form.
		$( '#ai-lib-form-3' ).submit(function(e) {
			e.preventDefault();
		
			var formSettings = {
				aihidden : '',
				codesystem :'',
				codeinput :'',
				nameinput:'',
				action:''
			};
			formSettings.aihidden = $( "input[name='ai-health-lib-hidden']" ).val();
			formSettings.codesystem = $( "input[name='code-system-lab']" ).val();
			formSettings.codeinput = $( "input[name='code-input-lab']" ).val();
			formSettings.nameinput = $( "input[name='name-input-lab']" ).val();
			formSettings.action = 'ai_lab_test';
			
			
			$.ajax({
				url: lib_ajax.ajaxurl,
				type: 'POST',
				data: formSettings,
				success: function (response) {
					$( '#ai-health-lib-response' ).replaceWith(response);
				},
				error: function(response) {
					$( '#ai-lib-form-3' ).reset();
				}
			});
			
		});
		return false;
	}); 
})( jQuery );