<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://axiosinteractive.net
 * @since      1.0.0
 *
 * @package    Ai_Health_Lib
 * @subpackage Ai_Health_Lib/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Ai_Health_Lib
 * @subpackage Ai_Health_Lib/public
 * @author     Chris Woolf <chris.woolf91@gmail.com>
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Include the main Problem Response class.
if ( ! class_exists( 'Ai_Health_Lib_Problem' ) ) {
	include_once dirname( __FILE__ )  . '/class-ai-health-lib-problem.php';
}

// Include the main Drug Response class.
if ( ! class_exists( 'Ai_Health_Lib_Drug' ) ) {
	include_once dirname( __FILE__ )  . '/class-ai-health-lib-drug.php';
}

// Include the main Drug Response class.
if ( ! class_exists( 'Ai_Health_Lib_Lab' ) ) {
	include_once dirname( __FILE__ )  . '/class-ai-health-lib-lab.php';
}

class Ai_Health_Lib_Public {
	
	/**
	* These are public base variables used for MedlinePlus
	* Concatenate code variables and code system variables to these.
	*
	*
	*/
	
	protected $problem_response_class;
	protected $drug_response_class;
	protected $lab_response_class;
	
	public $code_system;
	public $code_typed;
	
	protected $problem_form_section;
	protected $drug_form_section;
	protected $lab_form_section;
	
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		
		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->problem_response_class = new Ai_Health_Lib_Problem( $plugin_name, $version );
		$this->drug_response_class = new Ai_Health_Lib_Drug( $plugin_name, $version );
		$this->lab_response_class = new Ai_Health_Lib_Lab( $plugin_name, $version );
	}

	/**
	 * Register the style sheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ai_Health_Lib_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ai_Health_Lib_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/ai-health-lib-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ai_Health_Lib_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ai_Health_Lib_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( 'ai_health_lib_ajax', plugin_dir_url( __FILE__ ) . 'js/ai-health-lib-public.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/jquery.form.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, 'https://code.jquery.com/jquery-3.2.1.min.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( 'ai_health_lib_ajax', 'lib_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );	
	}
	
	/**
	 * Storing HTML for problem form.
	 *
	 * @since  1.0.0
	 *
	 */
	protected function display_problem_form(  ) {
		$this->problem_form_section .= sprintf( '<h3>Problem (Diagnosis) Code: </h3>
		<form id="ai-lib-form-1" method="POST" style="margin:2px;padding:2px;">
            <div class="form-group"><small class="form-text text-muted" style="margin:1px;padding:1px;font-size:14px;">Select a code system to use.</small>
				<input type="hidden" name="ai-health-lib-hidden" value="yes">
                <div class="form-check" style="margin:2px;padding:2px;"><label class="form-check-label"><input id="icd10" name="code-system-prob" class="form-check-input" type="radio" value=90> ICD-10-CM</label></div>
                <div class="form-check" style="margin:2px;padding:2px;"><label class="form-check-label"><input id="icd9" name="code-system-prob" class="form-check-input" type="radio" value=103> ICD-9-CM</label></div>
                <div class="form-check" style="margin:2px;padding:2px;"><label class="form-check-label"><input id="snomed" name="code-system-prob" class="form-check-input" type="radio" value=96> SNOMED CT</label></div>
					<small class="form-text text-muted" style="margin:1px;padding:1px;font-size:14px;">Use J45.40 to try it out!</small>
				</div>
                <input name="code-input-prob" role="textbox" class="form-control" type="text" placeholder="Type code here"><br>
				<button class="btn btn-outline-primary" type="submit" style="margin:2px;">GO</button>
		</form>');
		return $this->problem_form_section;
	}
	
	/**
	 * Storing HTML for drug form.
	 *
	 * @since  1.0.0
	 *
	 */
	protected function display_drug_form(  ) {
		$this->drug_form_section .= sprintf( '<h3>Drug Medication Code:</h3>
        <form id="ai-lib-form-2" method="POST" style="margin:2px;padding:2px;">
            <div class="form-group"><small class="form-text text-muted" style="margin:1px;padding:1px;font-size:14px;">Select a code system to use.</small>
			<input type="hidden" name="ai-health-lib-hidden" value="yes">
            <div class="form-check" style="margin:2px;padding:2px;"><label class="form-check-label"><input name="code-system-drug" class="form-check-input" type="radio" value=88> RXCUI</label></div>
            <div class="form-check" style="margin:2px;padding:2px;"><label class="form-check-label"><input name="code-system-drug" class="form-check-input" type="radio" value=69> NDC</label></div>
				<small class="form-text text-muted" style="margin:1px;padding:1px;font-size:14px;">Use 387013 to try it out!</small>
			</div>
			<div class="input-group">
				<input name="code-input-drug" role="textbox" class="form-control" type="text" placeholder="Type code here"><br>
				<strong>AND/OR You can also enter the drug name:</strong><br>
				<input name="name-input-drug" role="textbox" class="form-control" type="text" placeholder="Type drug name here"><br>
				<button name="ai-health-lib-submit-drug" class="btn btn-outline-primary" type="submit" style="margin:2px;">GO</button>
			</div>
		</form>');
		return $this->drug_form_section;
	}
	
	/**
	 * Storing HTML for lab form.
	 *
	 * @since  1.0.0
	 *
	 */
	protected function display_lab_form(  ) {
		$this->lab_form_section .= sprintf( '<h3>Lab Test Code:</h3>
        <form id="ai-lib-form-3" method="POST" style="margin:2px;padding:2px;">
			<input type="hidden" name="ai-health-lib-hidden" value="yes">
            <div class="form-group"><small class="form-text text-muted" style="margin:1px;padding:1px;font-size:14px;">Select a code system to use.</small>
            <div class="form-check" style="margin:2px;padding:2px;"><label class="form-check-label"><input name="code-system-lab" class="form-check-input" type="radio" value=""> LOINC</label></div>
				<small class="form-text text-muted" style="margin:1px;padding:1px;font-size:14px;">Use 1668-3 to try it out!</small>
			</div>
			<div class="input-group">
				<input name="code-input-lab" role="textbox" class="form-control" type="text" placeholder="Type code here"><br>
				<strong>OPTIONAL:</strong><br>
				<input name="name-input-lab" role="textbox" class="form-control" type="text" placeholder="Type lab name here"><br>
				<button name="ai-health-lib-submit-lab" class="btn btn-outline-primary" type="submit" style="margin:2px;">GO</button>
			</div>
		</form>');
		return $this->lab_form_section;
	}
	
	/**
	 * Linking all forms together in one html string.
	 *
	 * @since  1.0.0
	 *
	 */
	public function display_form_page( $content = null ) {
		$render = '';
		$render = sprintf('
		<div class="container-fluid" style="margin:2px;padding:2px;">
			<div id="ai-health-lib-response" class="row">
				<div class="col">');
		$render .= $this->display_problem_form(); 
		$render .= $this->display_drug_form(); 
		$render .= $this->display_lab_form();
		$render .= sprintf('
				</div>
			</div>
		</div>'
		);
		//$render .= apply_filters( 'the_content', $content );
		echo $render;
	}
	
	/**
	 * Not sure if I want to use this yet...
	 *
	 * @since  1.0.0
	 *
	 */
	public function get_form_page( ) {
		include_once dirname( __FILE__ ) . '/partials/ai-health-lib-public-display.php';
	}
	
	/**
	 * Gets and stores problem response in class var.
	 *
	 * @since  1.0.0
	 *
	 */
	public function display_problem_response( $code_system, $code_input, $post_id ) {
		return $this->problem_response_class->get_problem_response( $code_system, $code_input, $post_id );
	}
	
	/**
	 * Gets and stores drug response in class var.
	 *
	 * @since  1.0.0
	 *
	 */
	public function display_drug_response( $code_system, $code_input, $drug_name,$post_id ) {
		return $this->drug_response_class->get_drug_response( $code_system, $code_input, $drug_name, $post_id );
	}
	
	/**
	 * Gets and stores lab response in class var.
	 *
	 * @since  1.0.0
	 *
	 */
	public function display_lab_response( $code_input, $lab_name, $post_id ) {
		return $this->lab_response_class->get_lab_response( $code_input, $lab_name, $post_id );
	}
	
	/**
	 * Render the lab response
	 *
	 * @since    1.0.0
	 */
	public function ai_lab_test(  ) {
		$current_id = rand(0,20);
		$render_sections = sprintf('<div class="%s-%s">', $this->plugin_name, $current_id);
		if ( $_POST["aihidden"] === "yes" ) {
			$render_sections .= $this->display_lab_response( $_POST["codeinput"], $_POST["nameinput"], $current_id );
		} else {
			$render_sections .= '<p>It does not look like we could find the data.</p>';
		}
		$render_sections .= '</div>';
		echo $render_sections;
		die();
	}
	
	/**
	 * Render the drug response
	 *
	 * @since    1.0.0
	 */
	public function ai_drug_test(  ) {
		$current_id = rand(0,20);
		$render_sections = sprintf('<div class="%s-%s">', $this->plugin_name, $current_id);
		if ( $_POST["aihidden"] === "yes" ) {
			$render_sections .= $this->display_drug_response( $_POST["codesystem"], $_POST["codeinput"], $_POST["nameinput"], $current_id );
		} else {
			$render_sections .= '<p>It does not look like we could find the data.</p>';
		}
		$render_sections .= '</div>';
		echo $render_sections;
		die();
	}
	
	/**
	 * Render the problem response
	 *
	 * @since    1.0.0
	 */
	public function ai_problem_test(  ) {
		$current_id = rand(0,20);
		$render_sections = sprintf('<div class="%s-%s">', $this->plugin_name, $current_id);
		if( $_POST["aihidden"] === "yes" ) {
			$render_sections .= $this->display_problem_response( $_POST["codesystem"], $_POST["codeinput"], $current_id );
		} else {
			$render_sections .= '<p>It does not look like we could find the data.</p>';
		}
		$render_sections .= '</div>';
		echo $render_sections;
		die();
	}
	
	/**
	 * Register shortcode with class-ai-health-lib.php
	 *
	 * @since    1.0.0
	 */
	public function add_ai_shortcode_init() {
		//add_shortcode( 'ai-health-lib-diagnosis', array( $this, 'ai_problem_test' ));
		//add_shortcode( 'ai-health-lib-drug', array( $this, 'ai_drug_test' ));
		add_shortcode( 'ai-health-lib', array( $this, 'display_form_page' ));
	}
	
	/**
	 * Get field values from form
	 *
	 * @since    1.0.0
	 */
	public function get_form_data( $_code_typed, $_code_system ) {
		$this->code_typed = $_code_typed;
		$this->code_system = $_code_system;
	}
}
