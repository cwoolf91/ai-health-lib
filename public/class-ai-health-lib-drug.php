<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://axiosinteractive.net
 * @since      1.0.0
 *
 * @package    Ai_Health_Lib
 * @subpackage Ai_Health_Lib/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Ai_Health_Lib
 * @subpackage Ai_Health_Lib/public
 * @author     Chris Woolf <chris.woolf91@gmail.com>
 */
 
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
 
class Ai_Health_Lib_Drug extends Ai_Health_Lib_Drug_Abs{
	
	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->initialize_base_vars();
		$this->initialize_default_vars();
	}

	/**
	 * Register the style sheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ai_Health_Lib_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ai_Health_Lib_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		//wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/ai-health-lib-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ai_Health_Lib_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ai_Health_Lib_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		//wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/ai-health-lib-public.js', array( 'jquery' ), $this->version, false );

	}
	
	/**
	 * Set the base vars for using Medline Plus API. Need to sanitize.
	 *
	 * @since    1.0.0
	 */
	
	protected function initialize_base_vars() {
		$this->base_medline_url = 'https://apps.nlm.nih.gov/medlineplus/services/mpconnect_service.cfm?';
		$this->base_code_system = 'mainSearchCriteria.v.cs=2.16.840.1.113883.6.';
		$this->base_actual_code = 'mainSearchCriteria.v.c=';
		$this->RXCUI = 88;
		$this->NDC = 69;
		$this->base_language_selection = 'informationRecipient.languageCode.c=';
		$this->base_response_type = 'knowledgeResponseType=';
		$this->response_type_json = 'application/json';
		$this->response_type_xml = 'text/xml';
		$this->base_name_url = 'mainSearchCriteria.v.dn=';
		$this->selected_code_system = $this->RXCUI;
		$this->named_drug = '';
	}
	
	/**
	 * Set the default vars for using Medline Plus API. This is not the same as base vars. Need to sanitize.
	 *
	 * @since    1.0.0
	 */
	
	protected function initialize_default_vars() {
		$this->language_selection = 'en';
		$this->base_language_selection = $this->base_language_selection . $this->language_selection;
		$this->set_default_response_type();
	}
	
	/**
	 * Set default response type to XML. Need to sanitize.
	 *
	 * @since    1.0.0
	 */
	
	protected function set_default_response_type() {
		$this->response_type_xml = $this->base_response_type . $this->response_type_xml;
		$this->response_type_json = $this->base_response_type . $this->response_type_json;
		// Get selected code system.
		//$this->selected_code_system = get_option( 'ai_health_lib_code_system' );
		//$this->set_actual_code_system( $this->selected_code_system );
	}
	
	/**
	 * Basic GET function. Testing
	 *
	 * @since    1.0.0
	 */
	
	public function get_drug_response( $code_system, $code_input = '', $named_drug = '', $post_id ) {
		
		$post_content = null;
		$check_code = null;
		
		// These can't BOTH be empty.
		// Setting up flags for performance maybe?
		if( $code_input == '' && $named_drug == '' ) {
			$post_content =  'There was no code received or drug name! Please try again..' . $post_content;
		} else {
			$this->code_system = trim($code_system);
			$this->named_drug = trim($named_drug);
			$this->actual_code = trim($code_input);
			
			/* Validate according to system.
			if($code_system == '96') {
				$check_code = $this->validate_code_input_SNOMED_CT($this->actual_code);
			}else ($code_system == '90') {
				$check_code = $this->validate_code_input_ICD_10($this->actual_code);
			}*/
			
			
			
			// Check code and return error if invalid.
			if( $check_code == 91 ) {
				$post_content =  'That code is not valid, please try again...' . $post_content;
			} else {

				$format_response_heading = null;
				$format_response_summary = null;
		
				$response_url = $this->base_medline_url . $this->base_code_system . $this->selected_code_system . 
					'&' . $this->base_actual_code . $this->actual_code .
					'&' . $this->base_name_url . $this->named_drug .
					'&' . $this->base_language_selection .
					'&' . $this->response_type_json;
				// Remove whitespace.
				trim($response_url);
		
				$drug_response_body = $this->get_response_code( $response_url );
		
				// Decode and save as an object
				$this->drug_json_obj = json_decode($drug_response_body);
		
		
				$format_response_heading = 
					sprintf('<div class="%s-heading-drug-%s">' .  
						'<strong>' . 'Title: ' . '</strong>' . $this->drug_json_obj->feed->title->_value . '<br>' .
						'<strong>' . 'Subtitle: ' . '</strong>' . $this->drug_json_obj->feed->subtitle->_value . '<br>' .
						'<strong>' . 'Author: ' . '</strong>' . $this->drug_json_obj->feed->author->name->_value . '<br>' .
						'<strong>' . 'Author URI: ' . '</strong>' . $this->drug_json_obj->feed->author->uri->_value . '<br>' .
						'</div>', $this->plugin_name, $post_id
					);
				
				// Just adds a unique id kind of.
				$current_summary = 0;
				foreach ( $this->drug_json_obj->feed->entry as $value ) {
					$current_summary++;
				
					$summary = '';
					// Check if summary is empty and add it. If empty, nothing shows, otherwise will have it.
					if( $value->summary->_value != "" ) {
						$summary = sprintf( '<strong>' . 'Summary: ' . '</strong>' . $value->summary->_value . '<br>' );
					}
				
					$format_response_summary .= sprintf( '<br>' . '<div class="%s-summary-drug-%s">', $this->plugin_name, $current_summary );
					$format_response_summary .= sprintf( 
						'<strong>' . 'Title: ' . '</strong>' . $value->title->_value . '<br>' .
						'<strong>' . 'Link: ' . '</strong>' . '<a class="drug-link" href="' . $value->link[0]->href . '">' . $value->link[0]->href . '</a>' . '<br>' .
						'<strong>' . 'Author: ' . '</strong>' . $value->author->name->_value . '<br>' .
						'%s' .
						'</div>', $summary
					);
				}
								
				$post_content .= $post_content . $format_response_heading .$format_response_summary;
			}
		}
		return $post_content;
	}
	
	/**
	 * Getting the response code after sending the request. Testing
	 * Need to validate further
	 * @since    1.0.0
	 */
	protected function get_response_code( $response_url ) {
		$drug_response = wp_remote_get( $response_url );
		$response_code = $drug_response["response"]["code"];
		
		if( $response_code == 200 ) {
			$drug_response_body = wp_remote_retrieve_body( $drug_response );
			return $drug_response_body;
		} else {
			return $response_code;
		}
	}
	
	/**
	 * Setter for code.
	 * 
	 * @since    1.0.0
	 */
	protected function set_actual_code( $code_to_set ) {
		$this->actual_code = $code_to_set;
	}
	
	/**
	 * Setter for code system. Default is RXCUI set in init func.
	 * 
	 * @since    1.0.0
	 */
	protected function set_actual_code_system( $code_system ) {
		// 4 = RXCUI
		// 5 = NDC

		switch ( $code_system ) {
			case 4:
				$this->selected_code_system = $this->RXCUI;
				break;
			case 5:
				$this->selected_code_system = $this->NDC;
				break;
			default:
				$this->selected_code_system = 92;
				break;
		}
	}

}
