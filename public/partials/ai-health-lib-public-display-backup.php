<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin. 
 * Author: Chris Woolf
 * Created with help of Bootstrap 4.0
 *
 * @link       https://axiosinteractive.net
 * @since      1.0.0
 *
 * @package    Ai_Health_Lib
 * @subpackage Ai_Health_Lib/public/partials
 *
 * This file should primarily consist of HTML with a little bit of PHP.
 */ 
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$response = 'no';
if( isset($_GET['code-system-prob']) ) {
	$response = 'yes';
	echo $response;
	exit;
}

?>
	<div class="container-fluid" style="margin:2px;padding:2px;">
        <div id="ai-health-lib-response" method="POST" class="row">
            <div class="col">
                <h3>Problem (Diagnosis) Code: </h3>
                <form id="ai-lib-form-1" style="margin:2px;padding:2px;">
                    <div class="form-group"><small class="form-text text-muted" style="margin:1px;padding:1px;font-size:14px;">Select a code system to use.</small>
                        <div class="form-check" style="margin:2px;padding:2px;"><label name="ai-health-label" class="form-check-label"><input id="icd10" name="code-system-prob" class="form-check-input" type="radio" value=90>ICD-10-CM</label></div>
                        <div class="form-check" style="margin:2px;padding:2px;"><label name="ai-health-label" class="form-check-label"><input id="icd9" name="code-system-prob" class="form-check-input" type="radio" value=103>ICD-9-CM</label></div>
                        <div class="form-check" style="margin:2px;padding:2px;"><label name="ai-health-label" class="form-check-label"><input id="snomed" name="code-system-prob" class="form-check-input" type="radio" value=96>SNOMED CT</label></div>
						<small class="form-text text-muted" style="margin:1px;padding:1px;font-size:14px;">Use J45.40 to try it out!</small>
					</div>
                    <input name="code-input-prob" role="textbox" class="form-control" type="text" placeholder="Type code here"><button class="btn btn-outline-primary" type="submit" style="margin:2px;">GO</button>
				</form>
            </div>
            <div class="col">
                <h3>Drug Medication Code:</h3>
                <form id="ai-lib-form-2" style="margin:2px;padding:2px;">
                    <div class="form-group"><small class="form-text text-muted" style="margin:1px;padding:1px;font-size:14px;">Select a code system to use.</small>
						<input type="hidden" name="ai-health-lib-hidden" value="yes">
                        <div class="form-check" style="margin:2px;padding:2px;"><label name="code-system-drug" class="form-check-label"><input class="form-check-input" type="radio">RXCUI</label>
						</div>
                        <div class="form-check" style="margin:2px;padding:2px;"><label name="code-system-drug" class="form-check-label"><input class="form-check-input" type="radio">NDC</label>
						</div>
						<small class="form-text text-muted" style="margin:1px;padding:1px;font-size:14px;">Use 387013 to try it out!</small>
					</div>
					<input name="code-input-drug" role="textbox" class="form-control" type="text" placeholder="Type code here"><button name="ai-health-lib-submit-drug" class="btn btn-outline-primary" type="submit" style="margin:2px;">GO</button>
				</form>
            </div>
            <div class="col">
                <h3>Lab Test Code:</h3>
                <form id="ai-lib-form-3" style="margin:2px;padding:2px;">
					<input type="hidden" name="ai-health-lib-hidden" value="yes">
                    <div class="form-group"><small class="form-text text-muted" style="margin:1px;padding:1px;font-size:14px;">Select a code system to use.</small>
                        <div class="form-check" style="margin:2px;padding:2px;"><label class="form-check-label"><input name="code-system-lab" class="form-check-input" type="radio">LOINC</label>
						</div>
						<small class="form-text text-muted" style="margin:1px;padding:1px;font-size:14px;">Use 1668-3 to try it out!</small>
					</div>
					<input name="code-input-drug" role="textbox" class="form-control" type="text" placeholder="Type code here"><button name="ai-health-lib-submit-lab" class="btn btn-outline-primary" type="submit" style="margin:2px;">GO</button>
				</form>
            </div> 
        </div>
	</div>
	<script src="http://localhost/axios-wp-dev/wp-content/plugins/ai-health-lib/public/js/ai-health-lib-public.js"></script>
	<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
