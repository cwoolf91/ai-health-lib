<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin. 
 * Author: Chris Woolf
 * Created with help of Bootstrap 4.0
 *
 * @link       https://axiosinteractive.net
 * @since      1.0.0
 *
 * @package    Ai_Health_Lib
 * @subpackage Ai_Health_Lib/public/partials
 *
 * This file should primarily consist of HTML with a little bit of PHP.
 */ 
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
	<div class="container-fluid" style="margin:2px;padding:2px;">
        <div id="ai-health-lib-response" class="row">
            <div class="col">
                
            </div> 
        </div>
	</div>
	<script src="http://localhost/axios-wp-dev/wp-content/plugins/ai-health-lib/public/js/ai-health-lib-public.js"></script>
	<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
